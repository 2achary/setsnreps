export const API_URL = 'http://localhost:8000/api/';
// export const API_URL = 'http://138.68.57.124/api/';

export const HEADERS = {
  "Content-Type": "application/json",
  "Authorization": "Token " + JSON.parse(localStorage.getItem('api-token'))
}

export function new_set(exerciseid, previous, weight, reps, sessionid, order) {
  return fetch(`${API_URL}set/new-set/`, {
    method: "POST",
    headers: HEADERS,
    body: JSON.stringify({
      exercise_id: exerciseid,
      previous: previous,
      weight: weight,
      reps: reps,
      session_id: sessionid,
      order: order
    })
  })
      .then(r => r.json())
}

export function getTableFriendlySets(session_id) {
  return fetch(`${API_URL}set/table-friendly-set-list/?session_id=${session_id}`, {
    method: "GET",
    headers: HEADERS
  })
      .then(r => r.json())
}