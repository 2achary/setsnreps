import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import {API_URL} from "../helpers";
import '../css/Login.css'

export default class Login extends Component {
    //Set initial state
    state = {
        user: '',
        password: '',
        remember: false,
        goHome: false
    }

    userNameRef = React.createRef();
    passwordRef = React.createRef();

    // Update state whenever an input field is changed
    handleFieldChange = (e) => {
        const stateToChange = {}
        stateToChange[e.target.id] = e.target.value
        this.setState(stateToChange)
    }

    handleLogin = (e) => {
        e.preventDefault()
        fetch(`${API_URL}api-token-auth/`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({username: this.userNameRef.current.value, password: this.passwordRef.current.value}),
          credentials: 'include'
        }).then(r => r.json())
            .then((response) => {
                if (response.token) {
                    localStorage.setItem('api-token', JSON.stringify(response.token));
                    this.setState({goHome: true})
                } else {
                    console.log(response)
                }
            })
    }


    render() {
            if (this.state.goHome) {
                return <Redirect to="/"/>
            }
            return (
                <div className={"container main-container"}>
                  <form onSubmit={this.handleLogin} className="login-form">
                    <h1 className="h3 mb-3 font-weight-normal">Please sign
                      in</h1>

                    <div className="form-group">
                      <label htmlFor="exampleInputEmail1">User Name</label>
                      <input ref={this.userNameRef}
                             onChange={this.handleFieldChange}
                             type="text" className="form-control"
                             id="exampleInputEmail1"
                             placeholder="Enter username"/>

                    </div>
                    <div className="form-group">
                      <label htmlFor="exampleInputPassword1">Password</label>
                      <input
                          ref={this.passwordRef}
                          onChange={this.handleFieldChange}
                          type="password" className="form-control"
                          id="exampleInputPassword1" placeholder="Password"/>
                    </div>

                    <button type="submit" className="btn btn-primary">Sign In
                    </button>
                  </form>
                </div>
            )
        }
    }

