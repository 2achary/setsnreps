import React, {Component} from "react";
import {new_set} from "../helpers";
import ExerciseTable from "./ExerciseTable";
import {API_URL} from "../helpers";
import {getTableFriendlySets} from '../helpers'

export default class EmptyWorkout extends Component {
  state = {
    exercises: [],
    exercise: null,
    session: {},
    sets: []
  }
  handleChange = (event) => {
    this.setState(
        {
          [event.currentTarget.name]: event.currentTarget.value
        }
    )
  }

  refreshSetsForSessionID = () => {
    return getTableFriendlySets(this.props.sessionId)
        .then(response => {
          this.setState({sets: response});
        })
  };

  onSubmit = (e) => {
    e.preventDefault();
    this.newSetFromExisting(this.state.exercise);
  };

  componentDidMount() {
    getTableFriendlySets(this.props.sessionId)
        .then(response => {
          this.setState({sets: response});
        })
    // create a new workout and set it to state
    fetch(`${API_URL}session/${this.props.sessionId}/`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Token " + JSON.parse(localStorage.getItem("api-token"))
      }
    })
        .then(r => r.json())
        .then(response => {
          this.setState({session: response});
        });
    // get the list of all exercises and save them to state
    fetch(`${API_URL}exercises/`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Token " + JSON.parse(localStorage.getItem("api-token"))
      }
    })
        .then(r => r.json())
        .then(response => {
          this.setState({exercises: response.results});
        });
  }

  newSetFromExisting = (exercise_id) => {
    return fetch(`${API_URL}new-set-from-existing/`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Token " + JSON.parse(localStorage.getItem("api-token"))
      },
      body: JSON.stringify({
        exercise_id: exercise_id,
        session_id: this.state.session.id
      })
    })
        .then(r => r.json())
        .then(response => {
          this.refreshSetsForSessionID();
        });
  }

  toggleTitleEdit = () => {
      this.setState({editTitle: !this.state.editTitle})
  }

  onChange = (event) => {
    const sessionCopy = {
      ...this.state.session,
      [event.currentTarget.name]: event.currentTarget.value
    }
    this.setState({session: sessionCopy});
  }

  saveTitle = () => {
    fetch(`${API_URL}session/${this.props.sessionId}/`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Token " + JSON.parse(localStorage.getItem("api-token"))
      },
      body: JSON.stringify({ name: this.state.session.name })
    })
        .then(res => res.json())
        .then((res) => {
          this.refreshSetsForSessionID()
        }).then(() => {
          this.toggleTitleEdit();
    });
  }

  render() {
    let optionItems = this.state.exercises.map(exercise => {
      return <option key={exercise.id}
                     value={exercise.id}>{exercise.name}</option>
    })
    optionItems.unshift(<option key='blank' value='Select Exercise'>Select
      Exercise</option>)



    return (
        <div className={"container main-container"}>
          <div className={"top-bar"}>
            <a href="/"><button className={"btn btn-light btn-sm"}>back</button></a>
          </div>
          {
            this.state.editTitle?
              <div><input name="name" onChange={this.onChange} value={this.state.session.name}/> <button onClick={this.saveTitle} className={"btn btn-outline-primary btn-sm"}>save</button></div>:
              <h3 onClick={this.toggleTitleEdit}>{this.state.session.name}</h3>
          }
          {
            this.state.sets.map((set) => {
              return <ExerciseTable
                  key={set.exercise_id}
                  newSetFromExisting={this.newSetFromExisting}
                  refreshSetsForSessionID={this.refreshSetsForSessionID}
                  exerciseDetails={set}></ExerciseTable>
            })
          }
          <hr/>
          <select className={"custom-select exercise-select"} type="text" name="exercise" onChange={this.handleChange}>
            {
              optionItems
            }
          </select>
          <button className={"btn btn-block btn-outline-primary btn-sm exercise-select-button"} onClick={this.onSubmit}>Add Exercise</button>

        </div>
    );
  }
}
