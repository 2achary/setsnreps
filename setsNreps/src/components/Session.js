import React, {Component} from "react";
import {new_set} from "../helpers";
import ExerciseTable from "./ExerciseTable";
import {getTableFriendlySets} from '../helpers'

export default class Session extends Component {
  state = {
    exercises: [],
    exercise: null,
    session: {},
    sets: [],
    tableFriendlySessions: []
  }

  componentDidMount() {
    getTableFriendlySets(this.props.session.id).then((response) => {
      this.setState({tableFriendlySessions: response})
    })
  }

  render() {

    return (
        <div className={"session-summary-card"}>

          <div className="card session-card" style={{width: '18rem'}}>
            <div className="card-body">
              <h6 className="card-title">{this.props.session.name}</h6>
              <p className="card-subtitle mb-2 text-muted">{this.props.session.date_created}</p>
              {
                this.state.tableFriendlySessions.length? this.state.tableFriendlySessions.map((exerciseDetails) => {
                  return <p className="card-text">{`${exerciseDetails.sets ? exerciseDetails.sets.length : 0} x ${exerciseDetails.exercise_name}`}</p>
                }): ''
              }
                < a href={`/session/${this.props.session.id}`}
                  className="card-link"><button className={'btn btn-sm btn-light edit-button'}> Edit</button></a>
            </div>
          </div>

        </div>
    );
  }
}
