import React, {Component} from 'react';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import BootstrapTable from 'react-bootstrap-table-next';
import {API_URL} from "../helpers";
import cellEditFactory, { Type } from 'react-bootstrap-table2-editor';

export default class ExerciseTable extends Component {

  columns = [
    {
      dataField: 'weight',
      text: 'lbs'
    },
    {
      dataField: 'reps',
      text: 'Reps'
    },
    {
      dataField: 'complete',
      text: 'Complete',
      formatter: (cellContent, row) => (
          <div className="checkbox">
            <label>
              <input type="checkbox" checked={ row.complete } />
            </label>
          </div>
        )
    }];

  updateCell = (oldValue, newValue, row, column) => {
    if (column.dataField === 'weight' || column.dataField === 'reps') {
      newValue = parseInt(newValue);
    }

    if (column.dataField == 'complete') {
      if (newValue === 'false') {
        newValue = false;
      } else {
        newValue = true;
      }
    }
    fetch(`${API_URL}sets/${row.id}/`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Token " + JSON.parse(localStorage.getItem("api-token"))
      },
      body: JSON.stringify({ [column.dataField]: newValue })
    })
        .then(res => res.json())
        .then((res) => {
          this.props.refreshSetsForSessionID()
        });
  }

  beforeCellSave = (oldValue, newValue, row, column) => {}

  render() {
    return (
        <div className={"exercise-table"}>
          <div className="react-bootstrap-table">
            <h5>{this.props.exerciseDetails.exercise_name}</h5>
            <BootstrapTable
                keyField="id"
                classes="table-sm table-borderless"
                data={this.props.exerciseDetails.sets}
                columns={this.columns}
                cellEdit={cellEditFactory({
                  mode: 'click',
                  afterSaveCell: this.updateCell,
                  beforeSaveCell: this.beforeCellSave,
                  onStartEdit: this.onStartEdit,
                  blurToSave: true

                })}
                bordered={ false }
                hover

            />
            <button className={"btn btn-block btn-sm btn-outline-secondary"} onClick={() => this.props.newSetFromExisting(this.props.exerciseDetails.exercise_id)} >Add Set</button>
          </div>
        </div>
    );
  }
}