import React, {Component} from 'react';
import Session from './Session'
import {API_URL} from "../helpers";
import {Redirect} from 'react-router-dom'

export default class HomePage extends Component {
  state = {
    sessions: [],
    newSession: null
  }

  componentDidMount() {

    // get the list of all exercises and save them to state
    fetch(`${API_URL}sessions/`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Token " + JSON.parse(localStorage.getItem("api-token"))
      }
    })
        .then(r => r.json())
        .then(response => {
          this.setState({sessions: response.results});
        });
  }

  createNewWorkout = () => {
    // create a new workout and set it to state
    fetch(`${API_URL}session/new-workout/`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Token " + JSON.parse(localStorage.getItem("api-token"))
      },
      body: JSON.stringify({ name: "Abs and stuff" })
    })
      .then(r => r.json())
      .then(response => {
        this.setState({ newSession: response });
      });
  }

  logout = () => {
    localStorage.removeItem('api-token');
    this.setState({logout: true})
  }

  render() {
    if (this.state.logout) {
      return <Redirect to={'login'}/>
    }
    if (this.state.newSession) {
      return <Redirect to={`/session/${this.state.newSession.id}`}/>
    }
    return (
        <div className={"container main-container"}>
          <div className={"top-bar"}>
            <button onClick={this.logout} className={"btn btn-sm btn-light"}>logout</button>
          </div>
          <h1>Start Workout</h1>
          <h3>Quick Start</h3>

            <button onClick={this.createNewWorkout} className={"btn btn-block btn-outline-primary btn-sm"}>Create New
              Workout
            </button>

          {
            this.state.sessions.map((session) => {
              return <Session key={session.id} session={session}/>
            })
          }

        </div>
    )
  }
};

